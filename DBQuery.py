""" Projet Job DashBoard : Groupe 2 
Module by Victor & Julien
SQL by Victor
DB by Victor
Intégration by Julien """

import psycopg2, psycopg2.extras
import DBProtocol


def count_all_offres():
    x = DBProtocol.ReqSQL(""" SELECT COUNT(*) FROM taboffres""", None)[0][0]
    print(">>> count_all_offres() =", x)
    return x


def count_all_offres_on_line():
    x = DBProtocol.ReqSQL("select count(*) from TabOffres WHERE en_ligne IS TRUE", None)[0][0]
    print(">>> count_all_offres() =", x)
    return x


def get_all_offres(title_search="", search_where="libellé", page=1, limit=3, order_by="price", sort="descending") -> list:
    sort_values = {"descending": "DESC", "ascending": "ASC"}

    q = f"""SELECT * FROM taboffres
            WHERE LOWER({search_where}) LIKE LOWER(%(title_search)s)
            ORDER BY {order_by} {sort_values.get(sort, sort_values["descending"])}
            LIMIT %(limit)s
            OFFSET %(offset)s
        """

    p = {
        "title_search": "%" + title_search + "%",
        "search_where": search_where,
        "limit": limit,
        "offset": (page-1) * limit,
    }

    return DBProtocol.ReqSQL(q, p)


def count_offres(title_search = "", search_where = "libellé") -> int:
    q = f"""SELECT COUNT(*) FROM taboffres
            WHERE LOWER({search_where}) LIKE LOWER(%(title_search)s)
        """

    p = {
        "title_search": "%" + title_search + "%",
        "search_where": search_where,
    }

    return DBProtocol.ReqSQL(q, p)[0][0]
