import base64
from io import BytesIO

from matplotlib.figure import Figure
import matplotlib.pyplot as plt

import sqlalchemy
import pandas as pd
import numpy as np


import DBProtocol, DBQuery


def CreaPng():
    with BytesIO() as buf:
        plt.savefig(buf, format="png", bbox_inches='tight')
        NomPngTampon = base64.b64encode(buf.getbuffer()).decode("ascii")
        return NomPngTampon

#plot_colortable(mcolors.TABLEAU_COLORS, "Tableau Palette", sort_colors=False, emptycols=2)

def CamTypeContrat():
    ReqSQL = DBProtocol.ReqSQL("""SELECT type_contrat FROM taboffres;""", None)

    CDI = CDD = Interim = Prof_lib = non_traite = 0

    for offres in ReqSQL:
        if "indéterminée" in offres[0]:
            CDI += 1
        elif "déterminée" in offres[0]:
            CDD += 1
        elif "intérimaire" in offres[0]:
            Interim += 1
        elif "libérale" in offres[0]:
            Prof_lib += 1
        else:
            non_traite += 1

    sizes = {"CDI": CDI, "CDD": CDD, "Interim": Interim, "Freelance": Prof_lib, "Non renseigné": non_traite}
    labels = ""
    empty = []

    for key, value in sizes.items():
        if value == 0:
            empty.append(key)        
        else:
            labels += key
            
    for key in empty:
        del sizes[key]
        
    labels = list(sizes.keys())
    data = list(sizes.values())

    ### Création du camembert ###
        
    fig1, ax1 = plt.subplots(figsize=(8, 8))
    ax1.pie(data, labels=labels, autopct='%1.1f%%')
    ax1.axis('equal')
    ax1.legend(sizes, title="Contrats", loc="upper left", bbox_to_anchor=(1.1, 0, 0, 1))
    
    return CreaPng()


def graph_regions():
    dept_file = "DocUtile/departements-francais.csv"
    engine = sqlalchemy.create_engine('postgresql://dashboard:job@localhost:5432/job_dashboard')

    df_dept = pd.read_sql("dept", engine)
    df_offres = pd.read_sql("taboffres", engine)

    df_regions = df_dept["REGION"].drop_duplicates()

    df_dept_region = df_dept[["NUMERO","REGION"]]
    df_dept_region.columns = ['dept', 'REGION']

    df_offres["dept"] = df_offres.code_postal.str[:2]
    df_offres_dept = df_offres[["dept"]]

    df_table = pd.merge(df_offres_dept, df_dept_region, on = "dept", how = "left")
    df_graph = df_table["REGION"].value_counts()

    labels = list(df_graph.index)
    data = list(df_graph.values)

    ### Création du camembert ###

    fig1, ax1 = plt.subplots(figsize=(8, 8))
    ax1.pie(data, labels=labels, autopct='%1.1f%%')
    ax1.axis('equal')
    ax1.legend(labels, title="Région", loc="upper left", bbox_to_anchor=(1.3, 0, 0, 1))

    return CreaPng()


def CamTypeLangage():
    ReqSQL = DBProtocol.ReqSQL("""SELECT python, sql, data FROM taboffres""", None)
    c_Pyt = c_Sql = c_Dat = c_PS = c_PD = c_PSD = c_SD = c_NR = 0

    for offre in ReqSQL:
        if offre["python"] == True and offre["sql"] == True and offre["data"] == True:
            c_PSD += 1
        elif offre["python"] == True and offre["sql"] == True and offre["data"] == False:
            c_PS += 1
        elif offre["python"] == True and offre["sql"] == False and offre["data"] == True:
            c_PD += 1
        elif offre["python"] == True and offre["sql"] == False and offre["data"] == False:
            c_Pyt += 1
        elif offre["python"] == False and offre["sql"] == True and offre["data"] == True:
            c_SD += 1
        elif offre["python"] == False and offre["sql"] == True and offre["data"] == False:
            c_Sql += 1
        elif offre["python"] == False and offre["sql"] == False and offre["data"] == True:
            c_Dat += 1
        else:
            c_NR += 1

    sizes = {"Python": c_Pyt, "SQL": c_Sql, "Data": c_Dat, "Python & SQL": c_PS, "Python & Data": c_PD, "SQL & Data": c_SD, "Les trois": c_PSD, "Non renseigné": c_NR}
    labels = ""
    empty = []

    for key, value in sizes.items():
        if value == 0:
            empty.append(key)        
        else:
            labels += key
            
    for key in empty:
        del sizes[key]
        
    labels = list(sizes.keys())
    data = list(sizes.values())
    fig1, ax1 = plt.subplots(figsize=(8, 8))
    ax1.pie(data, labels=labels, autopct='%1.1f%%')
    ax1.axis('equal')
    ax1.legend(sizes, title="Programme promo", loc="upper left", bbox_to_anchor=(1.1, 0, 0, 1))
    
    return CreaPng()

def HistLangages():
    objects = ("Python", "SQL", "Linux", "HTML", "Javascript", "Git")
    y_pos = np.arange(len(objects))
    cat = {"Python": DBQuery.count_offres(title_search ="python", search_where = "corps"), 
            "SQL": DBQuery.count_offres(title_search ="sql", search_where = "corps"), 
            "Linux": DBQuery.count_offres(title_search ="linux", search_where = "corps"), 
            "HTML": DBQuery.count_offres(title_search ="html", search_where = "corps"), 
            "Javascript": DBQuery.count_offres(title_search ="javascript", search_where = "corps"), 
            "Git": DBQuery.count_offres(title_search ="git", search_where = "corps")} 
            
    data = list(cat.values())

    plt.subplots(figsize=(10, 6))
    plt.bar(y_pos, data, align='center')
    plt.xticks(y_pos, objects, fontsize = 12)
    plt.ylabel("Nombre d'offres", fontsize = 12)

    return CreaPng()