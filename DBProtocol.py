""" Projet Job DashBoard : Groupe 2 
Module by Julien
SQL by Victor
DB by Victor """

import psycopg2, psycopg2.extras, sys


def ConnexionBD():
    global conn
    global cur
    try:
        conn
    except NameError:
        try:
            conn = psycopg2.connect(dbname="job_dashboard", user="dashboard", password="job", host="localhost", port="5432")
        except:
            print("Connexion impossible. Application en panne call ghostbusters !")
            sys.exit()
    cur = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)
    print(conn, cur)
    return conn, cur


def Commit():
    conn.commit()


def DeconnexionBD():
    cur.close() 
    conn.close()


def ExeSQL(CodeSQL, MonTuple):
    cur.execute(CodeSQL, MonTuple)


def ReqSQL(CodeSQL, MonTuple):
    cur.execute(CodeSQL, MonTuple)
    return cur.fetchall()


def Del(table, condition, MonTuple):
    cur.execute(f"""DELETE FROM {table} WHERE {condition} = %s;""", MonTuple)


def UpdateBD(table, *colone, condition, MonTuple): # WIP
    print(colone)
    for i, kwarg in enumerate(colone):
        if i == len(colone):
            MesColones += kwarg + "= %s"
        else:
            MesColones += kwarg + "= %s,"
    print(MesColones)
    cur.execute(f"""UPDATE {table} SET {MesColones} WHERE {condition};""", MonTuple)


################ Manipulations des Tables ################

def DropTable(table):
    sql = f"""drop table if exists {table}"""
    cur.execute(sql)


################ Table principale ################

def CreaTable():
    sql = """CREATE TABLE TabOffres (
                PKID text, 
                date_publica date,
                en_ligne BOOLEAN,
                hors_ligne date,
                libellé text, 
                nom_entreprise text,
                source text, 
                code_postal text, 
                ville text, 
                lien_source text,
                corps TEXT,
                type_contrat TEXT,
                perti_auto smallint,
                python BOOLEAN,
                sql BOOLEAN,
                data BOOLEAN);"""

    cur.execute(sql)


def InsertTabOffres(dico):
    sql = """INSERT INTO TabOffres (
                PKID,
                date_publica,
                en_ligne,
                hors_ligne,
                libellé, 
                nom_entreprise, 
                source, 
                code_postal, 
                ville, 
                lien_source,
                corps,
                type_contrat,
                perti_auto,
                python,
                sql,
                data)
            VALUES (
                %(PKID)s,
                %(date_publica)s,
                %(en_ligne)s,
                %(hors_ligne)s,
                %(libellé)s,
                %(nom_entreprise)s,
                %(source)s,
                %(code_postal)s,
                %(ville)s,
                %(lien_source)s,
                %(corps)s,
                %(type_contrat)s,
                %(perti_auto)s,
                %(python)s,
                %(sql)s,
                %(data)s);"""

    cur.execute(sql, dico)


ConnexionBD()