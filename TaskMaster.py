""" Maître-esclave des robots de scrap """

""" Projet Job DashBoard : Groupe 2 
Module by Julien """

import DBProtocol, Robot_PoE, os

#os.system("sudo docker-compose up -d")

DBProtocol.ConnexionBD()

# DBProtocol.DropTable("TabOffres")
# DBProtocol.CreaTable()

Robot_PoE.main()

DBProtocol.DeconnexionBD()